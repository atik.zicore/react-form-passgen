import "./index.css";
import Header from "./components/Shared/Header/Header";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Homepage from "./components/Homepage/Home";
import Todopage from "./components/TodoPage/Todopage";
import Formpage from "./components/Formpage/Formpage";
import TestValidation from "./components/TestValidation/TestValidation";
import MultiValid from "./components/MultiValid/MultiValid";
import Masonry from "./components/Masonry/Masonry";

function App() {
  return (
    <div className="App  bg-[#0E172A]/90 text-white h-full w-full">
      {/* <BrowserRouter>
      <Header/>
      <Home/>
      </BrowserRouter> */}
      <Header />
      <div className="px-4 md:px-8 lg:px-12 xl:px-16">
        <Routes>
          <Route path="/" element={<Homepage />} />
          <Route path="/Todopage" element={<Todopage />} />
          <Route path="/Formpage" element={<Formpage />} />
          <Route path="/TestValidation" element={<TestValidation />} />
          <Route path="/MultiValid" element={<MultiValid />} />
          <Route path="/Masonry" element={<Masonry />} />
        </Routes>
      </div>
    </div>
  );
}

export default App;
