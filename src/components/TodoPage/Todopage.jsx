import React, { useEffect, useState } from "react";

const Todopage = () => {
  const [allTask, setAllTask] = useState([]);
  const [allDescription, setAllDescription] = useState([]);

  const [inputTask, setInputTask] = useState("");
  const [inputDescription, setInputDescription] = useState("");
  // for edit
  const [editedIndex, setEditedIndex] = useState(null);
  const [editedTask, setEditedTask] = useState("");
  const [editedDescription, setEditedDescription] = useState("");

  // Save form data
  const handleFormSubmit = (e) => {
    e.preventDefault();
    const newTask = [...allTask, inputTask];
    const newDescription = [...allDescription, inputDescription];
    setAllTask(newTask);
    setAllDescription(newDescription);
    localStorage.setItem("allTask", JSON.stringify(newTask));
    localStorage.setItem("allDescription", JSON.stringify(newDescription));
    setInputTask("");
    setInputDescription("");
  };

  useEffect(() => {
    const savedTask = JSON.parse(localStorage.getItem("allTask")) || [];
    const savedDescription = JSON.parse(localStorage.getItem("allDescription")) || [];
    setAllTask(savedTask);
    setAllDescription(savedDescription);
  }, []);

  // Delete Functionality
  const DeleteTask = (index) => {
    // console.log(index);
    const updatedTask = [...allTask];
    const updatedDescription = [...allDescription];
    updatedTask.splice(index, 1);
    updatedDescription.splice(index, 1);
    setAllTask(updatedTask);
    setAllDescription(updatedDescription);
    localStorage.setItem("allTask", JSON.stringify(updatedTask));
    localStorage.setItem("allDescription", JSON.stringify(updatedDescription));
  };

  // Edit Functionality
  const EditTask = (index) => {
    setEditedIndex(index);
    setEditedTask(allTask[index]);
    setEditedDescription(allDescription[index]);
    // console.log(index);
  };

  // Update Functionality
  const handleUpdateTask = (e) => {
    e.preventDefault();
    const updatedTask = [...allTask];
    const updatedDescription = [...allDescription];
    updatedTask[editedIndex] = editedTask;
    updatedDescription[editedIndex] = editedDescription;
    setAllTask(updatedTask);
    setAllDescription(updatedDescription);
    localStorage.setItem("allTask", JSON.stringify(updatedTask));
    localStorage.setItem("allDescription", JSON.stringify(updatedDescription));
    setEditedIndex();
  };

  return (
    <section className="w-full h-full">
      <div className="w-full md:w-1/2 lg:w-2/3 py-8 mx-auto">
        <h2 className="text-center text-base lg:text-lg font-bold capitalize">
          add task for doing work
        </h2>
        {/* form */}
        <div className="w-full h-full py-4 md:py-8">
          <form
            className="w-full h-full flex gap-x-4 justify-center "
            onSubmit={handleFormSubmit}
          >
            <div className="mb-3 w-full">
              <label htmlFor="taskTitle" className="capitalize">
                title
              </label>
              <input
                type="text"
                className="border border-black/50 placeholder:capitalize p-2 w-full placeholder:bg-transparent bg-transparent"
                placeholder="task ttile"
                onChange={(e) => setInputTask(e.target.value)}
                value={inputTask}
              />
            </div>
            <div className="mb-3 w-full">
              <label htmlFor="taskDescription" className="capitalize">
                title
              </label>
              <input
                type="text"
                className="border border-black/50 placeholder:capitalize p-2 w-full placeholder:bg-transparent bg-transparent"
                placeholder="task description"
                id="taskDexcription"
                onChange={(e) => setInputDescription(e.target.value)}
                value={inputDescription}
              />
            </div>
            <div className="mt-6 w-full">
              <label className=""></label>
              <input
                type="submit"
                className="bg-emerald-400 px-5 py-2 rounded-lg capitalize"
                value="add task"
              />
            </div>
          </form>
        </div>
        {/* Showing Data */}
        <section className="w-full h-full">
          <ul className="w-full h-full space-y-4">
            {allTask.map((todoItem, index) => {
              return (
                <li className="even:bg-gray-600 rounded-lg p-2" key={index}>
                  {editedIndex === index ? (
                    <div className="w-full h-full py-4 md:py-8">
                      <form
                        className="w-full h-full flex gap-x-4 justify-center "
                        onSubmit={handleUpdateTask}
                      >
                        <div className="mb-3 w-full">
                          <label htmlFor="taskTitle" className="capitalize">
                            title
                          </label>
                          <input
                            type="text"
                            className="border border-black/50 placeholder:capitalize p-2 w-full placeholder:bg-transparent bg-transparent"
                            placeholder="task ttile"
                            onChange={(e) => setEditedTask(e.target.value)}
                            value={editedTask}
                          />
                        </div>
                        <div className="mb-3 w-full">
                          <label
                            htmlFor="taskDescription"
                            className="capitalize"
                          >
                            title
                          </label>
                          <input
                            type="text"
                            className="border border-black/50 placeholder:capitalize p-2 w-full placeholder:bg-transparent bg-transparent"
                            placeholder="task description"
                            id="taskDexcription"
                            onChange={(e) =>
                              setEditedDescription(e.target.value)
                            }
                            value={editedDescription}
                          />
                        </div>
                        <div className="mt-6 w-full">
                          <label className=""></label>
                          <input
                            type="submit"
                            className="bg-emerald-400 px-5 py-2 rounded-lg capitalize"
                            value="Update task"
                          />
                        </div>
                      </form>
                    </div>
                  ) : (
                    <div
                      className="grid grid-cols-12 justify-center items-center w-full h-full even:bg-gray-600/20 p-4 rounded-xl"
                      key={index}
                    >
                      <div>
                        <p> {index + 1} .</p>
                      </div>
                      <div className="col-span-6 space-y-2">
                        <p className="capitalize font-bold text-lg">
                          {" "}
                          {todoItem}
                        </p>
                        <p className="capitalize ">{allDescription[index]}</p>
                      </div>
                      <div className="col-span-4 flex gap-4">
                        <button
                          type="button"
                          className="capitalize"
                          onClick={() => EditTask(index)}
                        >
                          edit
                        </button>
                        <button
                          type="button"
                          className="capitalize"
                          onClick={() => DeleteTask(index)}
                        >
                          delete
                        </button>
                      </div>
                    </div>
                  )}
                </li>
              );
            })}
          </ul>
        </section>
      </div>
    </section>
  );
};

export default Todopage;
