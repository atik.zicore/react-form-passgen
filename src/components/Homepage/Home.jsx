import React from "react";
import Hero from "./HomepageComponents/Hero";
import Section2 from "./HomepageComponents/Section2";
import Section3 from "./HomepageComponents/Section3";
import Section4 from "./HomepageComponents/Section4";

const Home = () => {
  return (
    <section className="w-full h-full">
     <Hero/>
     <Section2/>
     <Section3/>
     <Section4/>
    </section>

// {/* <section className="w-full h-full">
// <div className="flex justify-center items-center   ">
//   <div className="w-full h-full ">
//     <article>
//       Lorem Ipsum is simply dummy text of the printing and typesetting
//       industry. Lorem Ipsum has been the industry's standard dummy text
//       ever since the 1500s, when an unknown printer took a galley of type
//       and scrambled it to make a type specimen book. It has survived not
//       only five centuries, but also the leap into electronic typesetting,
//       remaining essentially unchanged. It was popularised in the 1960s
//       with the release of Letraset sheets containing Lorem Ipsum passages,
//       and more recently with desktop publishing software like Aldus
//       PageMaker including versions of Lorem Ipsum.
//     </article>
//   </div>
//   <div className="w-full h-full lg:col-span-2">
//     <img src="/Images/Classroom-cuate.svg" alt="classromm" />
//   </div>
// </div>
// </section> */}
  );
};

export default Home;
