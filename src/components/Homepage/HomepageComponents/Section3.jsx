import React from 'react'

const Section3 = () => {
  return (
    <div className='flex justify-center items-center basis-full'>
        <div className='basis-full lg:basis-1/2'>
        <article >
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime quisquam ullam atque voluptatem quam temporibus natus? Est vitae, unde tempore officia quae eius totam recusandae consectetur autem sint animi minima. Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quasi reiciendis quod iusto eaque nulla praesentium repellat soluta nostrum doloribus commodi qui tempora possimus tenetur, quo veritatis neque dolorem rem expedita.
        </article>
        </div>
        <div className='basis-full lg:basis-1/2'>
        <img src="/Images/indoor plants-bro.svg" alt="" className=''/>
        </div>
       
    </div>
  )
}

export default Section3