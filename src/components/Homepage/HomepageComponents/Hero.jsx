import React from 'react'

const Hero = () => {
  return (
    <div>
         <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 justify-center items-center  ">
        <div className="w-full h-full lg:h-[30rem] py-8 leading-relaxed tracking-wide text-base lg:text-lg place-content-center ">
          <article>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. It has survived not
            only five centuries, but also the leap into electronic typesetting,
            remaining essentially unchanged. It was popularised in the 1960s
            with the release of Letraset sheets containing Lorem Ipsum passages,
            and more recently with desktop publishing software like Aldus
            PageMaker including versions of Lorem Ipsum.
          </article>
        </div>
        <div className="w-full h-full lg:h-[30rem] lg:col-span-2">
          <img src="/Images/Classroom-cuate.svg" alt="classromm" className="w-full h-full" />
        </div>
      </div>
    </div>
  )
}

export default Hero