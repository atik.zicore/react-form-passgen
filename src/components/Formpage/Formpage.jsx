import React, { useState } from "react";
import { useForm } from "react-hook-form";

const Formpage = () => {
  const [page, setPage] = useState(0);
  // form validation
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
  } = useForm();
  const handleFormSubmit = (data) => {
    alert(JSON.stringify(data, null, 5))
  };


  const FormTitles = [
    "Personal info",
    "Education Background",
    "Programming Experience",
  ];
  return (
    <section className="w-full h-full ">
      <div className="w-2/3 mx-auto py-6 ">
        <h2 className="text-white font-bold capitalize py-2 text-lg text-center">
          fill all information
        </h2>
        <section className="grid grid-cols-3 gap-8 justify-center items-center bg-slate-900 rounded-lg p-8">
          {/* Form */}
          <div className=" col-span-2 w-full h-full">
            <h1 className="text-2xl lg:text-3xl font-bold capitalize">
              {" "}
              {FormTitles[page]}
            </h1>
            <form
              className="space-y-4 my-10"
              onSubmit={handleSubmit(handleFormSubmit)}
              method="POST"
            >
              {/* Personal Information */}
              <section className={page === 0 ? "block space-y-4" : "hidden"}>
                <div className="space-y-3">
                  <label htmlFor="userName" className="capitalize">
                    Name
                  </label>
                  <input
                    type="text"
                    placeholder="Enter your name"
                    id="userName"
                    name="userName"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                    {...register("userName", {
                      required: "Name is required",
                      pattern: {
                        value: /^[A-Za-z]+$/i,
                        message: "Invalid Name",
                      },
                    })}
               
                  />
                  <p className="text-xs text-red-600 bg-yellow-400">
                    {" "}
                    {errors.userName?.message}
                  </p>
                </div>
                <div className="space-y-3">
                  <label htmlFor="userEmail" className="capitalize">
                    email
                  </label>
                  <input
                    {...register("userEmail", {
                      required: "Email is Required",
                      pattern: {
                        value: /^[A-Za-z0-9-_]+@[A-Za-z0-9]+\.[A-Za-z]{2,}$/i,
                        message: "Invalid Email",
                      },
                    })}
                    type="text"
                    placeholder="Enter your email address"
                    id="userEmail"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                  {errors.userEmail && (
                    <p className="text-xs text-red-700">
                      {errors.userEmail?.message}
                    </p>
                  )}
                </div>

                <div className="space-y-3">
                  <label htmlFor="userMobile" className="">
                    Mobile
                  </label>
                  <input
                    {...register("userMobile", {
                      required: "Mobile number is required",
                     
                    })}
                    type="tel"
                    placeholder="Enter mobile number"
                    id="userMobile"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                  <p className="text-xs text-red-500">{errors.userMobile?.message}</p>
                </div>
                <div className="space-y-3">
                  <label htmlFor="userPassword" className="capitalize">
                    password
                  </label>
                  <input
                    {...register("userPassword")}
                    type="password"
                    placeholder="enter password"
                    id="userPassword"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>
                <div className="space-y-3">
                  <label htmlFor="userConfPassword" className="capitalize">
                    Confirm password
                  </label>
                  <input
                    {...register("userConfPassword", {
                      validate: {
                        matchesPassword: (value) => 
                        value === watch("userPassword") || "Password do not match"
                      }
                    })}
                    type="password"
                    placeholder="enter password"
                    id="userConfPassword"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                  <p className="text-red-700">
                    {errors.userConfPassword?.message}
                  </p>
                </div>
              </section>
              {/* Educational Information */}
              <section className={page === 1 ? "block space-y-4" : "hidden"}>
                <div className="space-y-3">
                  <label htmlFor="educationLvl" className="capitalize">
                    Enter Highest Level of Education
                  </label>
                  <input
                    type="text"
                    placeholder="highest level of education"
                    id="educationLvl"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>
                <div className="space-y-3">
                  <label htmlFor="institutueName" className="capitalize">
                    Name of the last Institution Attended
                  </label>
                  <input
                    type="text"
                    placeholder="Enter your institute name"
                    id="institutueName"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>

                <div className="space-y-3">
                  <label htmlFor="graduationYear" className="">
                    Graduation Year
                  </label>
                  <input
                    type="number"
                    placeholder="Enter graduation year"
                    id="graduationYear"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>
              </section>
              {/* Programming information */}
              <section className={page === 2 ? "block space-y-4" : "hidden"}>
                <div className="space-y-3">
                  <label htmlFor="programmingLang" className="capitalize">
                    Programming Languages Known
                  </label>
                  <input
                    type="text"
                    placeholder="highest level of education"
                    id="programmingLang"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>
                <div className="space-y-3">
                  <label htmlFor="experienceYear" className="capitalize">
                    Years of Programming Experience
                  </label>
                  <input
                    type="text"
                    placeholder="Enter your years of experience"
                    id="experienceYear"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>

                <div className="space-y-3">
                  <label htmlFor="portfolioLink" className="">
                    Portfolio Link
                  </label>
                  <input
                    type="text"
                    placeholder="Enter portfolio link"
                    id="portfolioLink"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>
              </section>
              {/* Button */}
              <div className="py-6 flex justify-between items-baseline">
                {page === 0 ? (
                  <button
                    type="button"
                    className={`capitalize px-4 py-2 bg-gray-600 basis-1/3 rounded-xl cursor-not-allowed`}
                  >
                    prev
                  </button>
                ) : (
                  <button
                    type="button"
                    className={`capitalize px-4 py-2 bg-blue-600 basis-1/3 rounded-xl `}
                    onClick={() => setPage(page - 1)}
                  >
                    prev
                  </button>
                )}

                {page === FormTitles.length - 1 ? (
                  <input
                    type="submit"
                    className="capitalize px-4 py-2 bg-blue-600 basis-1/3 rounded-xl"
                    value="submit"
                  />
                ) : (
                  <button
                  type="button"
                  className={ " capitalize px-4 py-2 bg-blue-600 basis-1/3 rounded-xl"}
                 
                  onClick={() => setPage(page + 1)}
                >next </button>
                )}
              </div>
            </form>
          </div>
          {/* Step */}
          <div className="w-full h-full bg-[#14B8A6] rounded-lg">
            <div className="flex gap-4 items-center justify-start p-4">
              <div className="border-white border rounded-full h-8 w-8">
                <p className="text-xl font-bold   flex justify-center">1</p>
              </div>
              <div>
                <h2 className="font-bold uppercase text-lg">step 1</h2>
                <h1>personal info</h1>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>
  );
};

export default Formpage;
