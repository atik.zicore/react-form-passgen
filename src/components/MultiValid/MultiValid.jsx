import React, { useEffect, useState } from "react";

const MultiValid = () => {
  const [page, setPage] = useState(0);
  // form validation
  const [userName, setUserName] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const [userMobile, setUserMobile] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [userConfPassword, setUserConfPassword] = useState("");
  const [allFieldsFilled, setAllFieldsFilled] = useState(false);
  //
  const [educationLvl, setEducationLvl] = useState("");
  const [instituteName, setInstituteName] = useState("");
  const [graduationYear, setGraduationYear] = useState(null);
  //
  const [programmingLang, setProgrammingLang] = useState("");
  const [experienceYear, setExperienceYear] = useState(0);
  const [portfolioLink, setPortfolioLink] = useState("");

  useEffect(() => {
    setAllFieldsFilled(
      userName !== "" &&
        userEmail !== "" &&
        userMobile !== "" &&
        userPassword !== "" &&
        userConfPassword !== ""
    );
  }, [userName, userEmail, userMobile, userPassword, userConfPassword]);

  const handleFormSubmit = (e) => {
    e.preventDefault();
    // Log form data
    const formData = {
      userName,
      userEmail,
      userMobile,
      userPassword,
      userConfPassword,
      educationLvl,
      instituteName,
      graduationYear,
      programmingLang,
      experienceYear,
      portfolioLink,
    };
    // console.log(formData);
    // alert(formData)
    alert(JSON.stringify(formData, null, 5));
  };

  const FormTitles = [
    "Personal info",
    "Education Background",
    "Programming Experience",
  ];
  return (
    <section className="w-full h-full ">
      <div className="w-2/3 mx-auto py-6 ">
        <h2 className="text-white font-bold capitalize py-2 text-lg text-center">
          fill all information
        </h2>
        <section className="grid grid-cols-3 gap-8 justify-center items-center bg-slate-900 rounded-lg p-8">
          {/* Form */}
          <div className=" col-span-2 w-full h-full">
            <h1 className="text-2xl lg:text-3xl font-bold capitalize">
              {" "}
              {FormTitles[page]}
            </h1>
            <div className="progressbar ">
              <div
                style={{
                  width: page === 0 ? "33.3%" : page === 1 ? "66.6%" : "100%",
                }}
              ></div>
            </div>

            <form
              className="space-y-4 my-10"
              onSubmit={handleFormSubmit}
              method="POST"
            >
              {/* Personal Information */}
              <section className={page === 0 ? "block space-y-4" : "hidden"}>
                <div className="space-y-3">
                  <label htmlFor="userName" className="capitalize">
                    Name
                  </label>
                  <input
                    type="text"
                    placeholder="Enter your name"
                    minLength={3}
                    maxLength={10}
                    id="userName"
                    name="userName"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                    onChange={(e) => setUserName(e.target.value)}
                    value={userName}
                  />
                  {userName.length < 5 ? (
                    <p className="text-xs text-red-500">
                      Name must be at least 5 characters
                    </p>
                  ) : (
                    <p className="text-emerald-400 text-xs">Name is valid</p>
                  )}
                </div>
                <div className="space-y-3">
                  <label htmlFor="userEmail" className="capitalize">
                    email
                  </label>
                  <input
                    onChange={(e) => setUserEmail(e.target.value)}
                    value={userEmail}
                    type="text"
                    placeholder="Enter your email address"
                    id="userEmail"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                  {/* {errors.userEmail && (
                    <p className="text-xs text-red-700">
                      {errors.userEmail?.message}
                    </p>
                  )} */}
                </div>

                <div className="space-y-3">
                  <label htmlFor="userMobile" className="">
                    Mobile
                  </label>
                  <input
                    onChange={(e) => setUserMobile(e.target.value)}
                    value={userMobile}
                    min={11}
                    max={11}
                    type="tel"
                    placeholder="Enter mobile number"
                    id="userMobile"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                  {userMobile.length < 11 ? (
                    <p className="text-xs text-red-500">
                      Mobile number is not valid
                    </p>
                  ) : (
                    ""
                  )}
                </div>
                <div className="space-y-3">
                  <label htmlFor="userPassword" className="capitalize">
                    password
                  </label>
                  <input
                    onChange={(e) => setUserPassword(e.target.value)}
                    value={userPassword}
                    type="password"
                    placeholder="enter password"
                    id="userPassword"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                    required
                    minLength={8}
                  />
                </div>
                <div className="space-y-3">
                  <label htmlFor="userConfPassword" className="capitalize">
                    Confirm password
                  </label>
                  <input
                    onChange={(e) => setUserConfPassword(e.target.value)}
                    value={userConfPassword}
                    type="password"
                    placeholder="enter password"
                    id="userConfPassword"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                  {userPassword !== userConfPassword &&
                    userConfPassword !== "" && (
                      <p className="text-xs text-red-500 capitalize">
                        password do not match
                      </p>
                    )}
                </div>
              </section>
              {/* Educational Information */}
              <section className={page === 1 ? "block space-y-4" : "hidden"}>
                <div className="space-y-3">
                  <label htmlFor="educationLvl" className="capitalize">
                    Enter Highest Level of Education
                  </label>
                  <input
                    onChange={(e) => setEducationLvl(e.target.value)}
                    value={educationLvl}
                    type="text"
                    placeholder="highest level of education"
                    id="educationLvl"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>
                <div className="space-y-3">
                  <label htmlFor="instituteName" className="capitalize">
                    Name of the last Institution Attended
                  </label>
                  <input
                    onChange={(e) => setInstituteName(e.target.value)}
                    value={instituteName}
                    type="text"
                    placeholder="Enter your institute name"
                    id="instituteName"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>

                <div className="space-y-3">
                  <label htmlFor="graduationYear" className="">
                    Graduation Year
                  </label>
                  <input
                    onChange={(e) => setGraduationYear(e.target.value)}
                    value={graduationYear}
                    type="number"
                    placeholder="Enter graduation year"
                    id="graduationYear"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>
              </section>
              {/* Programming information */}
              <section className={page === 2 ? "block space-y-4" : "hidden"}>
                <div className="space-y-3">
                  <label htmlFor="programmingLang" className="capitalize">
                    Programming Languages Known
                  </label>
                  <input
                    onChange={(e) => setProgrammingLang(e.target.value)}
                    value={programmingLang}
                    type="text"
                    placeholder="highest level of education"
                    id="programmingLang"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>
                <div className="space-y-3">
                  <label htmlFor="experienceYear" className="capitalize">
                    Years of Programming Experience
                  </label>
                  <input
                    onChange={(e) => setExperienceYear(e.target.value)}
                    value={experienceYear}
                    type="text"
                    placeholder="Enter your years of experience"
                    id="experienceYear"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>

                <div className="space-y-3">
                  <label htmlFor="portfolioLink" className="">
                    Portfolio Link
                  </label>
                  <input
                    onChange={(e) => setPortfolioLink(e.target.value)}
                    value={portfolioLink}
                    type="text"
                    placeholder="Enter portfolio link"
                    id="portfolioLink"
                    className="p-2 rounded-lg border border-gray-400 bg-transparent w-full  placeholder:capitalize col-span-3"
                  />
                </div>
              </section>
              {/* Button */}
              <div className="py-6 flex justify-between items-baseline">
                {/* Previous button */}
                {page === 0 ? (
                  <button
                    type="button"
                    className={`capitalize px-4 py-2 bg-gray-600 basis-1/3 rounded-xl cursor-not-allowed`}
                  >
                    prev
                  </button>
                ) : (
                  <button
                    type="button"
                    className={`capitalize px-4 py-2 bg-blue-600 basis-1/3 rounded-xl `}
                    onClick={() => setPage(page - 1)}
                  >
                    prev
                  </button>
                )}

                {/* Next Button */}
                {page === FormTitles.length - 1 ? (
                  <input
                    type="submit"
                    className="capitalize px-4 py-2 bg-blue-600 basis-1/3 rounded-xl"
                    value="submit"
                  />
                ) : (
                  <button
                    type="button"
                    className={`capitalize px-4 py-2 bg-blue-600 basis-1/3 rounded-xl ${
                      (page === 0 && allFieldsFilled && userName.length >= 5) && userMobile.length > 11 ||
                      (page === 1 &&
                        educationLvl !== "" &&
                        instituteName !== "" &&
                        graduationYear !== 0)
                        ? ""
                        : "cursor-not-allowed bg-gray-400"
                    }`}
                    onClick={() => {
                      if (
                        (page === 0 &&
                          allFieldsFilled &&
                          userName.length >= 5) ||
                        (page === 1 &&
                          educationLvl !== "" &&
                          instituteName !== "" &&
                          graduationYear !== 0)
                      ) {
                        setPage(page + 1);
                      }
                    }}
                    disabled={
                      !(
                        (page === 0 &&
                          allFieldsFilled &&
                          userName.length >= 5) ||
                        (page === 1 &&
                          educationLvl !== "" &&
                          instituteName !== "" &&
                          graduationYear !== 0)
                      )
                    }
                  >
                    next
                  </button>
                )}
              </div>
            </form>
          </div>
          <div className="w-full h-full bg-[#14B8A6] rounded-lg">
            <div className="flex gap-4 items-center justify-start p-4">
              <div className="border-white border rounded-full h-8 w-8">
                <p className="text-xl font-bold   flex justify-center">1</p>
              </div>
              <div>
                <h2 className="font-bold uppercase text-lg">step 1</h2>
                <h1>personal info</h1>
              </div>
            </div>
          </div>
        </section>
      </div>
    </section>
  );
};

export default MultiValid;
